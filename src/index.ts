import { ApolloServer } from 'apollo-server-express'
import express from 'express';
import { typeDefs } from './schema'
import { resolvers } from './resolvers'
import { context } from './context'

import { createServer } from 'http';
import { execute, subscribe } from 'graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { makeExecutableSchema } from '@graphql-tools/schema';


(async function () {
    const app = express();
    const httpServer = createServer(app);

    const schema = makeExecutableSchema({ typeDefs, resolvers });

    const subscriptionServer = SubscriptionServer.create(
        { schema, execute, subscribe },
        {
            server: httpServer, path: '/graphql'
        })

    const server = new ApolloServer({
        schema,
        context: req => ({
            ...req,
            prisma: context.prisma
        }),
        plugins: [{
            async serverWillStart() {
                return {
                    async drainServer() {
                        subscriptionServer.close();
                    }
                }
            }
        }]
    })

    await server.start();
    server.applyMiddleware({
        app,
        path: '/graphql'
    })

    await new Promise<void>(resolve => httpServer.listen({ port: 4000 }, resolve));
    console.log(`🚀 Server ready at: http://localhost:4000/graphql`);
})();
