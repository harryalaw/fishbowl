import { gql } from "apollo-server"
export const typeDefs = gql`

    type Query { 
        """Get words from a game"""
        getWords(gameId: ID!): [Word]
        """Get teams"""
        getTeams(gameId: ID!): Team
    }

    type Mutation {
        """Create a game"""
        createGame(playerId: ID!): Game
        """Join a game with the given ID"""
        joinGame(id: ID!): Game
        """Change a players name"""
        changeName(id: ID!): Player
        """Add a word to the current game"""
        addWord(word: String!, gameId: ID!): Word
    }

    """Object to represent a game of fishbowl"""
    type Game {
        """The ID of the game"""
        id: ID!
        """Plagers"""
        players: [Player!]
        """Teams"""
        teams: [Team!]!
        """Words"""
        words: [Word]
    }

    """Object representing a player"""
    type Player {
        """unique ID to identify the player"""
        id: ID!
        """Username provided by the player"""
        name: String!
        """The game that a player is in"""
        game: Game
    }

    """Object representing a team"""
    type Team {
        """The first team"""
        teamA: [Player!]
        """The second team"""
        teamB: [Player!]
        """The game that the team corresponds to"""
        game: Game
    }

    """Object to represent a word provided by a player"""
    type Word {
        """ID to represent the word"""
        id: ID!
        """The word/phrase provided by the player"""
        word: String!
        """The game that the word corresponds to"""
        game: Game
        """The player that provided the word"""
        player: Player
    }
`