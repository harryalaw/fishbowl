
import { Player } from "./player"
import { Game } from "./game"

export interface Team {
    teamA: Player[]
    teamB: Player[]
    game: Game
}