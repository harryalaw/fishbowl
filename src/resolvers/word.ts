import { Game } from "./game";
import { Player } from "./player"

export interface Word {
    id: String
    word: String
    game: Game
    player: Player
}