import { Game } from "./game"

export interface Player {
    id: String
    name: String
    game: Game
}
