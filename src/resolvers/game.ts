import { Player } from "./player"
import { Team } from "./team"
import { Word } from "./word"

import { context, Context } from "../context"

export interface Game {
    id: String
    players: Player[]
    teams: Team[]
    words: Word[]
}


export const gameResolvers = {
    Mutation: {
        createGame: (_parent: any, _args: { playerId: string }) => {
        },

        joinGame: {

        }
    },

    Game: {

    }
}
